package com.thitiphong;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class TestHashMap {
    public static void main(String[] args) {
        HashMap<String,String> map = new HashMap();
        map.put("A1","A12345");
        map.put("B1","A12345");
        map.put("C1","A12345");
        map.put("D1","A12345");
        Set<String> keys =  map.keySet();
        Iterator<String> Iterator = keys.iterator();
        while(Iterator.hasNext()){
            String key = Iterator.next();
            System.out.println(key + "=" +map.get(key));
        }
        System.out.println(map.get("A1"));
        System.out.println(map.isEmpty());

    }
}
